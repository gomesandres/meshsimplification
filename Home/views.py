# -*- coding: utf-8 -*-
__author__ = 'Andres'

from django.http import HttpResponseRedirect,HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext


def home(request,msg=''):
    data = {}
    return render_to_response('Three.js/index.html',data,
            context_instance=RequestContext(request))

def test(request,msg=''):
    data = {}
    return render_to_response('test.html',data,
            context_instance=RequestContext(request))


def old(request,msg=''):
    data = {}
    return render_to_response('old.html',data,
            context_instance=RequestContext(request))