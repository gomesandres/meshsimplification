from django.conf.urls import include, url
from django.contrib import admin
from Home.views import home,old,test

urlpatterns = [
    url(r'^$', home),
    url(r'^old', old),
    url(r'^test', test),
]
