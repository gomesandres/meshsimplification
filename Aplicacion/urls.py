from django.conf.urls import include, url
from django.contrib import admin
from Home import urls as Homeurls

urlpatterns = [
    # Examples:
    # url(r'^$', 'Aplicacion.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'', include(Homeurls)),
]
