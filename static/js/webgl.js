
    if ( ! Detector.webgl ) Detector.addGetWebGLMessage();
    var camera, scene, renderer, spotlight, hemiLight, rotObjectMatrix, canvas, static_url, gl, stats;
    var morphs = [];
    var clock = new THREE.Clock();
    var Dim=9.0;
    var TD, RTDim, Min, Max, Divisor;


    function ownCubic(x){return x*x*x;}

    function sleep(milliseconds) {
      var start = new Date().getTime();
      for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds){
          break;
        }
      }
    }

    function initGL(canvas) {
        try {
            gl = canvas.getContext("experimental-webgl");
            gl.viewportWidth = canvas.width;
            gl.viewportHeight = canvas.height;
            scene = new THREE.Scene();
            var WIDTH = window.innerWidth,
            HEIGHT = window.innerHeight;
            TD = document.getElementById("dimension").value;

            if(TD != "" && 0 < TD && TD < 40 ){
                Dim = TD;
            }
            
            RTDim = Math.floor(Math.sqrt(ownCubic(Dim)));

        } catch (e) {
        }
        if (!gl) {
            alert("Could not initialise WebGL, sorry :-(");
        }
    }

    function getShader(gl, id) {
      var shaderScript = document.getElementById(id);

      // error - element with supplied id couldn't be retrieved
      if (!shaderScript) {
        return null;
      }

      // If successful, build a string representing the shader source
      var str = "";
      var k = shaderScript.firstChild;
      while (k) {
        if (k.nodeType == 3) {
          str += k.textContent;
        }
        k = k.nextSibling;
      }

      var shader;

      // Create shaders based on the type we set
      //   note: these types are commonly used, but not required
      if (shaderScript.type == "x-shader/x-fragment") {
        shader = gl.createShader(gl.FRAGMENT_SHADER);
      } else if (shaderScript.type == "x-shader/x-vertex") {
        shader = gl.createShader(gl.VERTEX_SHADER);
      } else {
        return null;
      }

      gl.shaderSource(shader, str);
      gl.compileShader(shader);

      // Check the compile status, return an error if failed
      if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        console.log(gl.getShaderInfoLog(shader));
        return null;
      }

      return shader;
    }

    var shaderProgram;
    var RTTshader;
    var dimension;

    function initShaders() {
        var fragmentShader = getShader(gl, "per-fragment-lighting-fs");
        var vertexShader =  getShader(gl, "per-fragment-lighting-vs");
        shaderProgram = gl.createProgram();
        gl.attachShader(shaderProgram, vertexShader);
        gl.attachShader(shaderProgram, fragmentShader);
        gl.linkProgram(shaderProgram);
        if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
            alert("Could not initialise shaders");
        }

        gl.useProgram(shaderProgram);
        shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
        gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);
        shaderProgram.vertexNormalAttribute = gl.getAttribLocation(shaderProgram, "aVertexNormal");
        gl.enableVertexAttribArray(shaderProgram.vertexNormalAttribute);
        shaderProgram.textureCoordAttribute = gl.getAttribLocation(shaderProgram, "aTextureCoord");
        gl.enableVertexAttribArray(shaderProgram.textureCoordAttribute);
        shaderProgram.pMatrixUniform = gl.getUniformLocation(shaderProgram, "uPMatrix");
        shaderProgram.mvMatrixUniform = gl.getUniformLocation(shaderProgram, "uMVMatrix");
        shaderProgram.nMatrixUniform = gl.getUniformLocation(shaderProgram, "uNMatrix");
        shaderProgram.samplerUniform = gl.getUniformLocation(shaderProgram, "uSampler");
        shaderProgram.materialAmbientColorUniform = gl.getUniformLocation(shaderProgram, "uMaterialAmbientColor");
        shaderProgram.materialDiffuseColorUniform = gl.getUniformLocation(shaderProgram, "uMaterialDiffuseColor");
        shaderProgram.materialSpecularColorUniform = gl.getUniformLocation(shaderProgram, "uMaterialSpecularColor");
        shaderProgram.materialShininessUniform = gl.getUniformLocation(shaderProgram, "uMaterialShininess");
        shaderProgram.materialEmissiveColorUniform = gl.getUniformLocation(shaderProgram, "uMaterialEmissiveColor");
        shaderProgram.showSpecularHighlightsUniform = gl.getUniformLocation(shaderProgram, "uShowSpecularHighlights");
        shaderProgram.useTexturesUniform = gl.getUniformLocation(shaderProgram, "uUseTextures");
        shaderProgram.ambientLightingColorUniform = gl.getUniformLocation(shaderProgram, "uAmbientLightingColor");
        shaderProgram.pointLightingLocationUniform = gl.getUniformLocation(shaderProgram, "uPointLightingLocation");
        shaderProgram.pointLightingSpecularColorUniform = gl.getUniformLocation(shaderProgram, "uPointLightingSpecularColor");
        shaderProgram.pointLightingDiffuseColorUniform = gl.getUniformLocation(shaderProgram, "uPointLightingDiffuseColor");

        var fs = getShader(gl, "no_fs");
        var vs = getShader(gl, "no_vs");
        RTTshader = gl.createProgram();
        gl.attachShader(RTTshader, vs);
        gl.attachShader(RTTshader, fs);
        gl.linkProgram(RTTshader);
        if (!gl.getProgramParameter(RTTshader, gl.LINK_STATUS)) {
            alert("Could not initialise shaders");
        }

        gl.useProgram(RTTshader);
        RTTshader.VertexPos = gl.getAttribLocation(RTTshader, "VertexPos");
        gl.enableVertexAttribArray(RTTshader.VertexPos);
        RTTshader.max = gl.getUniformLocation(RTTshader, "max");
        RTTshader.min = gl.getUniformLocation(RTTshader, "min");
        RTTshader.Dim = gl.getUniformLocation(RTTshader, "Dim");
        RTTshader.RTDim = gl.getUniformLocation(RTTshader, "RTDim");

        gl.useProgram(shaderProgram);
    }

    function handleLoadedTexture(texture) {
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
        gl.bindTexture(gl.TEXTURE_2D, texture);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture.image);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

        gl.bindTexture(gl.TEXTURE_2D, null);
    }


    var moonTexture;
    var crateTexture;

    function initTextures() {
        crateTexture = gl.createTexture();
        crateTexture.image = new Image();
        crateTexture.image.onload = function () {
            handleLoadedTexture(crateTexture)
        };
        crateTexture.image.src = static_url+"img/crate.gif";
    }


    var mvMatrix = mat4.create();
    var mvMatrixStack = [];
    var pMatrix = mat4.create();

    function mvPushMatrix() {
        var copy = mat4.create();
        mat4.set(mvMatrix, copy);
        mvMatrixStack.push(copy);
    }

    function mvPopMatrix() {
        if (mvMatrixStack.length == 0) {
            throw "Invalid popMatrix!";
        }
        mvMatrix = mvMatrixStack.pop();
    }

    function setMatrixUniforms() {
        gl.useProgram(shaderProgram);
        gl.uniformMatrix4fv(shaderProgram.pMatrixUniform, false, pMatrix);
        gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, mvMatrix);

        var normalMatrix = mat3.create();
        mat4.toInverseMat3(mvMatrix, normalMatrix);
        mat3.transpose(normalMatrix);
        gl.uniformMatrix3fv(shaderProgram.nMatrixUniform, false, normalMatrix);
    }

    function degToRad(degrees) {
        return degrees * Math.PI / 180;
    }


    var rttFramebuffer;
    var textures = [];

    function initTextureFramebuffer() {
        var ext = gl.getExtension("WEBGL_draw_buffers");

        textures[0] = gl.createTexture();
        textures[1] = gl.createTexture();

        gl.bindTexture(gl.TEXTURE_2D, textures[0]);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, RTDim, RTDim, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);

        gl.bindTexture(gl.TEXTURE_2D, textures[1]);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, RTDim, RTDim, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);

        rttFramebuffer = gl.createFramebuffer();
        gl.bindFramebuffer(gl.FRAMEBUFFER, rttFramebuffer);
        rttFramebuffer.width = RTDim;
        rttFramebuffer.height = RTDim;

        var bufs = [];
        bufs[0] = ext.COLOR_ATTACHMENT0_WEBGL;
        bufs[1] = ext.COLOR_ATTACHMENT1_WEBGL;
        ext.drawBuffersWEBGL(bufs);

        gl.framebufferTexture2D(gl.FRAMEBUFFER, bufs[0], gl.TEXTURE_2D, textures[0], 0);
        gl.framebufferTexture2D(gl.FRAMEBUFFER, bufs[1], gl.TEXTURE_2D, textures[1], 0);

        gl.bindTexture(gl.TEXTURE_2D, null);
        gl.bindRenderbuffer(gl.RENDERBUFFER, null);
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    }


    var IndicesBuffer;

    function getBoundingBox(ver){
        Min = [0,0,0];
        Max = [0,0,0];
        var ind = [];

        for (var i = 0; i < ver.length; i+=3){
            var Vertex = [ver[i],ver[i+1],ver[i+2]];
            ind.push(i/3);
            ind.push(i/3);
            ind.push(i/3);
            Min[0] = (Min[0] > Vertex[0])? Vertex[0] : Min[0];
            Min[1] = (Min[1] > Vertex[1])? Vertex[1] : Min[1];
            Min[2] = (Min[2] > Vertex[2])? Vertex[2] : Min[2];
            
            Max[0] = (Max[0] < Vertex[0])? Vertex[0] : Max[0];
            Max[1] = (Max[1] < Vertex[1])? Vertex[1] : Max[1];
            Max[2] = (Max[2] < Vertex[2])? Vertex[2] : Max[2];

        }
        Divisor = [ Dim/(Max[0] - Min[0]), Dim/(Max[1] - Min[1]), Dim/(Max[2] - Min[2])];
        
        gl.useProgram(RTTshader);
        gl.uniform3f(RTTshader.max, Max[0], Max[1], Max[2]);
        gl.uniform3f(RTTshader.min, Min[0],Min[1],Min[2]);
        gl.uniform1f(RTTshader.Dim, Dim);
        gl.uniform1f(RTTshader.RTDim, RTDim);
        
        gl.useProgram(shaderProgram);

        return ind;
    }


    var cubeVertexPositionBuffer;
    var cubeVertexNormalBuffer;
    var cubeVertexTextureCoordBuffer;
    var cubeVertexIndexBuffer;

    var PlaneVertexPositionBuffer;
    var PlaneVertexNormalBuffer;
    var PlaneVertexTextureCoordBuffer;

    function initBuffers() {
        cubeVertexPositionBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexPositionBuffer);
            var vertices = [
                0.630493,0.000000,0.815278,
                0.369407,0.000000,0.815278,
                0.184703,0.000000,0.630557,
                0.184703,0.000000,0.369343,
                0.815197,0.000000,0.630557,
                0.815197,0.000000,0.369343,
                0.369407,0.000000,0.184722,
                0.000000,0.815297,0.630557,
                0.000000,0.815297,0.369343,
                0.000000,0.630593,0.184722,
                0.000000,0.369507,0.184722,
                0.000000,0.630593,0.815278,
                0.630493,0.184803,0.000000,
                0.369407,0.815297,0.000000,
                0.184703,0.630593,0.000000,
                0.184703,0.369507,0.000000,
                0.369407,0.184803,0.000000,
                0.815197,0.630593,0.000000,
                0.000000,0.369507,0.815278,
                0.000000,0.184803,0.630557,
                0.000000,0.184803,0.369343,
                0.630493,0.000000,0.184722,
                0.630493,0.815297,0.000000,
                0.815197,0.369507,0.000000,
                0.815197,1.000000,0.369343,
                0.630493,1.000000,0.184722,
                0.369407,1.000000,0.184722,
                0.184703,1.000000,0.369343,
                0.630493,1.000000,0.815278,
                0.369407,1.000000,0.815278,
                0.815197,1.000000,0.630557,
                1.000000,0.369507,0.184722,
                1.000000,0.184803,0.369343,
                1.000000,0.630593,0.815278,
                1.000000,0.815297,0.630557,
                1.000000,0.630593,0.184722,
                1.000000,0.369507,0.815278,
                1.000000,0.184803,0.630557,
                0.184703,1.000000,0.630557,
                0.630493,0.815297,1.000000,
                0.369407,0.815297,1.000000,
                0.815197,0.630593,1.000000,
                0.815197,0.369507,1.000000,
                0.184703,0.369507,1.000000,
                0.184703,0.630593,1.000000,
                0.369407,0.184803,1.000000,
                0.630493,0.184803,1.000000,
                1.000000,0.815297,0.369343
            ];

        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
        cubeVertexPositionBuffer.itemSize = 3;
        cubeVertexPositionBuffer.numItems = 56;
        cubeVertexNormalBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexNormalBuffer);
        var vertexNormals = getBoundingBox(vertices);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertexNormals), gl.STATIC_DRAW);
        cubeVertexNormalBuffer.itemSize = 3;
        cubeVertexNormalBuffer.numItems = vertexNormals.length;

        cubeVertexIndexBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cubeVertexIndexBuffer);
        var cubeVertexIndices = [
            0, 1, 2,      0, 2, 3,    // Front face
            4, 5, 6,      4, 6, 7,    // Back face
            8, 9, 10,     8, 10, 11,  // Top face
            12, 13, 14,   12, 14, 15 // Bottom face
    //        16, 17, 18,   16, 18, 19, // Right face
    //        20, 21, 22,   20, 22, 23  // Left face
        ];
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(cubeVertexIndices), gl.STATIC_DRAW);
        cubeVertexIndexBuffer.itemSize = 1;
        cubeVertexIndexBuffer.numItems = 24;

        PlaneVertexPositionBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, PlaneVertexPositionBuffer);
        vertices = [
             0.580687, 0.659, 0.813106,
            -0.580687, 0.659, 0.813107,
             0.580687, 0.472, 0.113121,
            -0.580687, 0.472, 0.113121
            ];
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
        PlaneVertexPositionBuffer.itemSize = 3;
        PlaneVertexPositionBuffer.numItems = 4;

        PlaneVertexNormalBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, PlaneVertexNormalBuffer);
        var vertexNormals = [
             0.000000, -0.965926, 0.258819,
             0.000000, -0.965926, 0.258819,
             0.000000, -0.965926, 0.258819,
             0.000000, -0.965926, 0.258819
        ];
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertexNormals), gl.STATIC_DRAW);
        PlaneVertexNormalBuffer.itemSize = 3;
        PlaneVertexNormalBuffer.numItems = 4;

        PlaneVertexTextureCoordBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, PlaneVertexTextureCoordBuffer);
        var textureCoords = [
            1.0, 1.0,
            0.0, 1.0,
            1.0, 0.0,
            0.0, 0.0
        ];
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(textureCoords), gl.STATIC_DRAW);
        PlaneVertexTextureCoordBuffer.itemSize = 2;
        PlaneVertexTextureCoordBuffer.numItems = 4;
    }


    var VertexPositionBuffer;
    var VertexNormalBuffer;
    var VertexTextureCoordBuffer;
    var VertexIndexBuffer;

    function handleModelData(Data) {
        if(Data == undefined) return false;

        var Normals, Vertex, Texture, Faces;

        if(Data.vertexNormals !==undefined){

            Vertex = Data.vertexPositions;
            Normals = Data.vertexNormals;
            Texture = Data.vertexTextureCoords;
            Faces = Data.indices;

        }else if(Data.vertices !== undefined){

            Vertex = Data.vertices;
            Normals = Data.normals;
            Texture = Data.uvs;            
            Faces = Data.faces;

        }

        VertexNormalBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, VertexNormalBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(Normals), gl.STATIC_DRAW);
        VertexNormalBuffer.itemSize = 3;
        VertexNormalBuffer.numItems = Normals.length / 3;

        //getBoundingBox(Vertex)

        VertexTextureCoordBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, VertexTextureCoordBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(Texture), gl.STATIC_DRAW);
        VertexTextureCoordBuffer.itemSize = 2;
        VertexTextureCoordBuffer.numItems = Texture.length / 2;

        VertexPositionBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, VertexPositionBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(Vertex), gl.STATIC_DRAW);
        VertexPositionBuffer.itemSize = 3;
        VertexPositionBuffer.numItems = Vertex.length / 3;

        VertexIndexBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, VertexIndexBuffer);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(Faces), gl.STREAM_DRAW);
        VertexIndexBuffer.itemSize = 1;
        VertexIndexBuffer.numItems = Faces.length;

        return true;
    }

    function loadModel() {
        var request = new XMLHttpRequest();
        request.open("GET", static_url+"files/objects/macbook.json");
        request.onreadystatechange = function () {
            if (request.readyState == 4) {
                handleModelData(JSON.parse(request.responseText));
            }
        };
        request.send();
    }


    function drawSceneOnLaptopScreen() {
        if(Divisor == undefined || Min == undefined || Dim == undefined || RTDim == undefined || !VertexPositionBuffer)return;

        gl.useProgram(RTTshader);
        gl.viewport(0, 0, rttFramebuffer.width, rttFramebuffer.height);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        gl.uniform3f(RTTshader.max, Max[0], Max[1], Max[2]);
        gl.uniform3f(RTTshader.min, Min[0],Min[1],Min[2]);
        gl.uniform1f(RTTshader.Dim, Dim);
        gl.uniform1f(RTTshader.RTDim, RTDim);

        gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexPositionBuffer);
        gl.vertexAttribPointer(RTTshader.VertexPos, cubeVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

        gl.drawElements(gl.POINTS, cubeVertexPositionBuffer.numItems, gl.UNSIGNED_SHORT, 0);

        gl.bindTexture(gl.TEXTURE_2D, textures[1]);
        gl.generateMipmap(gl.TEXTURE_2D);
        gl.bindTexture(gl.TEXTURE_2D, null);
        gl.useProgram(shaderProgram);
    }


    var laptopAngle = 0;

    function drawScene() {
        gl.useProgram(shaderProgram);
        gl.bindFramebuffer(gl.FRAMEBUFFER, rttFramebuffer);
        drawSceneOnLaptopScreen();

        gl.bindFramebuffer(gl.FRAMEBUFFER, null);

        gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        mat4.perspective(45, gl.viewportWidth / gl.viewportHeight, 0.1, 100.0, pMatrix);

        mat4.identity(mvMatrix);

        mvPushMatrix();

        mat4.translate(mvMatrix, [0, -0.4, -2.2]);
        mat4.rotate(mvMatrix, degToRad(-90), [1, 0, 0]);

        gl.uniform1i(shaderProgram.showSpecularHighlightsUniform, true);
        gl.uniform3f(shaderProgram.pointLightingLocationUniform, -1, 2, -1);

        gl.uniform3f(shaderProgram.ambientLightingColorUniform, 0.2, 0.2, 0.2);
        gl.uniform3f(shaderProgram.pointLightingDiffuseColorUniform, 0.8, 0.8, 0.8);
        gl.uniform3f(shaderProgram.pointLightingSpecularColorUniform, 0.8, 0.8, 0.8);

        // The laptop body is quite shiny and has no texture.  It reflects lots of specular light
        gl.uniform3f(shaderProgram.materialAmbientColorUniform, 1.0, 1.0, 1.0);
        gl.uniform3f(shaderProgram.materialDiffuseColorUniform, 1.0, 1.0, 1.0);
        gl.uniform3f(shaderProgram.materialSpecularColorUniform, 1.5, 1.5, 1.5);
        gl.uniform1f(shaderProgram.materialShininessUniform, 5);
        gl.uniform3f(shaderProgram.materialEmissiveColorUniform, 0.0, 0.0, 0.0);
        gl.uniform1i(shaderProgram.useTexturesUniform, false);

        if (VertexPositionBuffer) {
            gl.bindBuffer(gl.ARRAY_BUFFER, VertexPositionBuffer);
            gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, VertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

            gl.bindBuffer(gl.ARRAY_BUFFER, VertexTextureCoordBuffer);
            gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, VertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

            gl.bindBuffer(gl.ARRAY_BUFFER, VertexNormalBuffer);
            gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, VertexNormalBuffer.itemSize, gl.FLOAT, false, 0, 0);

            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, VertexIndexBuffer);
            setMatrixUniforms();
            gl.drawElements(gl.TRIANGLES, VertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
        }

        gl.uniform3f(shaderProgram.materialAmbientColorUniform, 0.0, 0.0, 0.0);
        gl.uniform3f(shaderProgram.materialDiffuseColorUniform, 0.0, 0.0, 0.0);
        gl.uniform3f(shaderProgram.materialSpecularColorUniform, 0.5, 0.5, 0.5);
        gl.uniform1f(shaderProgram.materialShininessUniform, 20);
        gl.uniform3f(shaderProgram.materialEmissiveColorUniform, 1.5, 1.5, 1.5);
        gl.uniform1i(shaderProgram.useTexturesUniform, true);

        gl.bindBuffer(gl.ARRAY_BUFFER, PlaneVertexPositionBuffer);
        gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, PlaneVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

        gl.bindBuffer(gl.ARRAY_BUFFER, PlaneVertexNormalBuffer);
        gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, PlaneVertexNormalBuffer.itemSize, gl.FLOAT, false, 0, 0);

        gl.bindBuffer(gl.ARRAY_BUFFER, PlaneVertexTextureCoordBuffer);
        gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, PlaneVertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, textures[1]);
        gl.uniform1i(shaderProgram.samplerUniform, 0);

        setMatrixUniforms();
        gl.drawArrays(gl.TRIANGLE_STRIP, 0, PlaneVertexPositionBuffer.numItems);

        mvPopMatrix();
    }

    function tick() {
        requestAnimFrame(tick);
        drawScene();
    }


    function webGLStart( url) {
        static_url = url;
        var canvas = document.getElementById("webgl");
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
        initGL(canvas);
        initTextureFramebuffer();
        initShaders();
        initBuffers();
        initTextures();
        loadModel();
        gl.clearColor(0.0, 0.0, 0.0, 1.0);
        gl.enable(gl.DEPTH_TEST);
        tick();
    }
