
export export function look(attribute,index,value){
    return (attribute[index] == value && attribute[index+1] == value && attribute[index+2] == value)
}

export function ownCubic(x){return x*x*x;}

export function setStatic(static_url){static = static_url;}

export function setXYZ(array,index,x,y,z){
    var i = index*3;
    array[i++]=x;
    array[i++]=y;
    array[i]=z;
}

export function rotateAroundObjectAxis(object, axis, radians) {
    rotObjectMatrix = new THREE.Matrix4();
    rotObjectMatrix.makeRotationAxis(axis.normalize(), radians);
    object.matrix.multiply(rotObjectMatrix);
    object.rotation.setEulerFromRotationMatrix(object.matrix);
}